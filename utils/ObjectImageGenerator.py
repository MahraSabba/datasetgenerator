import os
from random import randint
from utils.image_processing import read_image
from pathlib import Path

class ObjectImageGenerator(object):

    def __init__(self, location, image_per_object):
        self.object_image_limit = image_per_object
        self.base_image_location = location
        self.objects = []
        self.object_limit = {}
        self.object_images = {}
        self.object_size = {}
        self._get_images()
        self.object_count = len(self.objects)
        self.total_object_images = len(self.objects) * self.object_image_limit

    def get_total_images_to_create(self):
        return self.total_object_images

    def _get_images(self):
        """root is the base folder where sub-folders divide object images."""
        for root, directory, files in os.walk(self.base_image_location):
            if not files:
                continue

            object_class_name = os.path.basename(root)
            self.objects.append(object_class_name)
            self.object_limit[object_class_name] = self.object_image_limit
            for file in files:
                image_path = os.path.join(root, file)
                img = read_image(image_path)
                if img is None:
                    continue
                height, width = img.shape[:2]
                if not self.object_images:
                    self.object_images[object_class_name] = [img]
                    self.object_size[object_class_name] = [(width, height)]
                else:
                    if object_class_name not in self.object_images.keys():
                        self.object_images[object_class_name] = [img]
                        self.object_size[object_class_name] = [(width, height)]
                    else:
                        self.object_images[object_class_name].append(img)
                        self.object_size[object_class_name].append((width, height))

    def __call__(self):
        value = randint(0, self.object_count - 1)
        object_name = self.objects[value]
        if self.object_limit[object_name] > 0:
            self.object_limit[object_name] -= 1
            object_index = randint(0, len(self.object_images[object_name]) - 1)
            return self.object_images[object_name][object_index], \
                   self.object_size[object_name][object_index], \
                   object_name
        for object_name, object_images_left in self.object_limit.items():
            if object_images_left > 0:
                self.object_limit[object_name] -= 1
                object_index = randint(0, len(self.object_images[object_name]) - 1)
                return self.object_images[object_name][object_index], \
                       self.object_size[object_name][object_index], \
                       object_name
