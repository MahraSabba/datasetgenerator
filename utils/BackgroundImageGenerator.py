import os
from random import randint, shuffle
from utils.image_processing import read_image, verify_image_extension
from math import ceil


class BackgroundImageGenerator(object):

    def __init__(self, location, total_images):
        self.total_creatable_images = total_images
        self.base_location = location
        self.image_locations = {}
        self.total_images = 0
        self._get_images()
        self.max_callable_per_image = ceil(self.total_creatable_images/self.total_images)

    def _get_images(self):
        for root, directory, files in os.walk(self.base_location):
            if not files:
                continue
            for file in files:
                image_path = os.path.join(root, file)
                if verify_image_extension(image_path) is None:
                    continue
                self.image_locations[image_path] = 0
                self.total_images += 1

    def __call__(self):
        image_locations = [*self.image_locations]
        returnable_image_index = randint(0, len(image_locations)-1)
        returnable_image = image_locations[returnable_image_index]
        image = read_image(returnable_image)
        if image is None:
            del self.image_locations[returnable_image]
            self.total_images -= 1
            self.max_callable_per_image = ceil(self.total_creatable_images/self.total_images)
            return self.__call__()

        if self.image_locations[returnable_image] <= self.max_callable_per_image:
            self.image_locations[returnable_image] += 1
            return image

        for image_location in shuffle(image_locations):
            repetitions = self.image_locations[image_location]
            if repetitions >= self.max_callable_per_image:
                continue
            self.image_locations[image_location] += 1
            return image_location


