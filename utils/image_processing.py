from PIL import Image
import cv2


def verify_image_extension(image_location):
    if not image_location.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')):
        return None
    return image_location


def read_image(image_location):
    if verify_image_extension(image_location) is None:
        return None
    img = Image.open(image_location)
    try:
        img.verify()
        img.close()
        image = cv2.imread(image_location)
        return image
    except Exception as exc:
        return None