import os
from argparse import ArgumentParser
from utils import errors


def does_location_exists(location, location_name):
    if not os.path.exists(location):
        errors.exists(location_name)


def is_argument_none(arg, location_name):
    if arg is None:
        errors.is_none_error(location_name)


def is_a_directory(location, location_name):
    if not os.path.isdir(location):
        errors.not_a_directory(location_name)


def get_arguments():
    argparser = ArgumentParser()
    argparser.add_argument("-l",
                           "--object_dir",
                           dest="object_dir",
                           default="/home/trafficgenius/TrafficGenius/datasetgenerator/object_images",
                           help="Location of the folder with object images in it.")
    argparser.add_argument("-i",
                           "--images",
                           dest="bg_images",
                           default="/home/trafficgenius/TrafficGenius/datasetgenerator/bg_images",
                           help="Location of the folder with the background images in it.")
    argparser.add_argument("-c",
                           "--image_count",
                           dest="image_count",
                           default=100,
                           help="Number of images you want for each object")

    arguments = argparser.parse_args()

    object_directory = arguments.object_dir
    bg_directory = arguments.bg_images
    images_per_object = arguments.image_count

    is_argument_none(object_directory, "Object Directory")
    is_argument_none(bg_directory, "Background Directory")
    is_argument_none(images_per_object, "Images/Object")

    does_location_exists(object_directory, "Object Directory")
    does_location_exists(bg_directory, "Background Directory")

    is_a_directory(object_directory, "Object Directory")
    is_a_directory(bg_directory, "Background Directory")

    return object_directory, bg_directory, images_per_object
