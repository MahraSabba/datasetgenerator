from sys import exit
import logging


def exists(value):
    logging.exception("{} does not exists".format(value))
    exit(0)


def is_none_error(value):
    logging.exception("{} is None".format(value))
    exit(0)


def not_a_directory(value):
    logging.exception("{} is not a directory".format(value))
    exit(0)
