from utils import argument_parser
from utils.ObjectImageGenerator import ObjectImageGenerator
from utils.BackgroundImageGenerator import BackgroundImageGenerator
from app.Combiner import Combiner


def create_combiner():
    obj_dir, bg_dir, imgs_per_obj = argument_parser.get_arguments()
    obj_generator = ObjectImageGenerator(obj_dir, imgs_per_obj)
    bg_generator = BackgroundImageGenerator(bg_dir, obj_generator.get_total_images_to_create())
    combiner = Combiner(obj_generator, bg_generator)
    return combiner


if __name__ == '__main__':
    combiner = create_combiner()
    for i in range(10):
        combiner.create_transformed_image()
