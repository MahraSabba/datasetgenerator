from random import randint
from xml.etree import ElementTree as et
import cv2
from albumentations import Rotate, RandomBrightnessContrast, Blur, Normalize, Transpose, RandomCrop, MultiplicativeNoise, \
    ToSepia, JpegCompression, ChannelDropout, ChannelShuffle, Cutout, ToGray, VerticalFlip, HorizontalFlip, Flip, \
    RandomFog, Resize, RandomResizedCrop, ISONoise, ElasticTransform, Compose, OneOf, OneOrOther, RandomRotate90, \
    MotionBlur, MedianBlur


class Combiner(object):

    def __init__(self, obj_img_generator, bg_img_generator, max_objects_per_image=10, min_objects_per_image=4):
        self.max_objects_per_image = max_objects_per_image
        self.min_objects_per_image = min_objects_per_image
        self.obj_generator = obj_img_generator
        self.bg_generator = bg_img_generator
        self.bg_transform = self.bg_augmentation()
        self.normal_window = cv2.namedWindow("Image", cv2.WINDOW_NORMAL)

    def __call__(self):
        image, annotation = self.create_transformed_image()
        return image, annotation

    @staticmethod
    def bg_augmentation():
        return Compose([
            RandomRotate90(),
            Flip(),
            Transpose(),
            OneOf([
                MotionBlur(0.2),
                MedianBlur(blur_limit=3, p=0.1),
                Blur(blur_limit=3, p=0.1)
            ], p=0.2),
            RandomBrightnessContrast(),
            OneOf([
                ChannelShuffle(),
                ChannelDropout(),
                ToGray(),
                ToSepia()
            ], p=0.4)
        ])

    @staticmethod
    def resize(bg_size):
        scale_factor = (randint(1, 500) / 100)
        if scale_factor < 1:
            scale_type = cv2.INTER_LINEAR
        else:
            scale_type = cv2.INTER_CUBIC
        return Resize(int(bg_size[1]) * scale_factor, int(bg_size[0] * scale_factor), scale_type, 0.5)

    def apply_transform(self, bg_image, bg_size, det_im_dict, object_count):
        image = bg_image
        annotation = et.Element("annotation")
        resizer = self.resize(bg_size)
        image = resizer(image=image)
        bg_t_image = self.bg_transform(image=image)
        while True:
            cv2.imshow("Image", bg_t_image["image"])
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        return image, annotation

    def create_transformed_image(self):
        img_objects = {}
        no_of_objects = randint(self.min_objects_per_image, self.max_objects_per_image)
        for object_index in range(0, no_of_objects):
            ob_image, ob_size, ob_class = self.obj_generator()

            img_objects[object_index] = {"image": ob_image,
                                         "size": ob_size,
                                         "class": ob_class}

        bg_image = self.bg_generator()
        bg_size = bg_image.shape[:2]

        image, annotation = self.apply_transform(bg_image, bg_size, img_objects, no_of_objects)
        return image, annotation